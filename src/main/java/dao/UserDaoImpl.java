package dao;

import bean.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {
    Connection con;
    Statement stmt;
    ResultSet rs;
    User user;
    List list=new ArrayList();


    public List<User> allUser() {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con= DriverManager.getConnection("jdbc:mysql://localhost:3306/satDatabase","root","1234");

            stmt=con.createStatement();
            rs=stmt.executeQuery("select * from user");
            System.out.println("inside Dao try");
            while(rs.next())
            {
                System.out.println(rs.getString(1));
                user=new User(rs.getString(1),rs.getString(3));
                list.add(user);
            }
            stmt.close();
            con.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }
}
