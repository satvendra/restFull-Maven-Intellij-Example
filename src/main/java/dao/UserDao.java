package dao;

import bean.User;

import java.util.List;

public interface UserDao {

    public List<User> allUser();

}
